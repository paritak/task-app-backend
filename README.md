# task-app-backend

This application demonstrate rest-ful API to fetch data from database using ORM (SqlAlchemy) to serve the response in json format.

## Database and frameworks

- Postgres database
- Flask rest-ful Framework
- ORM (SqlAlchemy)

## Installations

* Python3 : https://realpython.com/installing-python/

* Postgres: http://www.postgresqltutorial.com/install-postgresql/

* Install python dependencies use requirements.txt


```sh
    $ cd to project directory
    $ pip install -r requirements.txt
```

## Configuration

Change the configuration in the following file:

#### app/config/dbconfig.py

```
POSTGRES_CONFIG = {
    "user": "<username for postgres user>",
    "password": "<password for user>",
    "database-name": "<database name>",
    "host": "<host name or IP address for database>",
    "port": "<database port number>"
    }
```

```
FILE_CONFIG = {
   "file_path": "<file path for project root directory + app/data_files/adult.data.csv>"
}
```

## Run application


```sh
    $ cd to project directory
    $ python3 run.py
```

It will run application on host = 0.0.0.0 and port = 5050, can change this settings in ```run.py``` file

Can test GET api as follow:

- http://localhost:5050/adults/summary

- http://127.0.0.1:5050/adults/summary

- http://<your-ip-address>:5050/adults/summary


