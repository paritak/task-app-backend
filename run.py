from flask import Flask
from flask_restful import Api
#from flask_cors import CORS
from resources import *
from app.dbconnection import connect_to_db, insert_from_file


app = Flask(__name__)
#cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
api = Api(app)

for each in resources:
    exec("api.add_resource("+each[0]+",'"+("','".join(each[1:]))+"')")

if __name__ == '__main__':
    connect_to_db(app)
    insert_from_file()
    app.run(host='0.0.0.0', port=5050)




