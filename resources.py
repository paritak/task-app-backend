from app.modules import *
from app.modules.dashboard import *

resources = [
    ["SummaryController", '/adults/summary'],
    ["ListController", '/adults/list']
]

