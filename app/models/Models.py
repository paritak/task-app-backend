from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

db = SQLAlchemy()


class Adults(db.Model):
    """A Adults class"""

    __tablename__ = "adults"

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    age = sqlalchemy.Column(sqlalchemy.Integer)
    workclass = sqlalchemy.Column(sqlalchemy.String(30))
    fnlwgt = sqlalchemy.Column(sqlalchemy.Integer)
    education = sqlalchemy.Column(sqlalchemy.String(30))
    education_num = sqlalchemy.Column(sqlalchemy.Integer)
    marital_status = sqlalchemy.Column(sqlalchemy.String(50))
    occupation = sqlalchemy.Column(sqlalchemy.String(50))
    relationship = sqlalchemy.Column(sqlalchemy.String(30))
    race = sqlalchemy.Column(sqlalchemy.String(30))
    sex = sqlalchemy.Column(sqlalchemy.String(10))
    capital_gain = sqlalchemy.Column(sqlalchemy.Integer)
    capital_loss = sqlalchemy.Column(sqlalchemy.Integer)
    hours_per_week = sqlalchemy.Column(sqlalchemy.Integer)
    native_country = sqlalchemy.Column(sqlalchemy.String(30))
    predicted_value = sqlalchemy.Column(sqlalchemy.String(20))

    def __init__(self, age=None, workclass=None, fnlwgt=None, education=None, education_num=None,
                 marital_status=None, occupation=None, relationship=None, race=None, sex=None,
                 capital_gain=None, capital_loss=None, hours_per_week=None, native_country=None,
                 predicted_value=None):
        self.age = age
        self.workclass = workclass.strip()
        self.fnlwgt = fnlwgt
        self.education = education.strip()
        self.education_num = education_num
        self.marital_status = marital_status.strip()
        self.occupation = occupation.strip()
        self.relationship = relationship.strip()
        self.race = race.strip()
        self.sex = sex.strip()
        self.capital_gain = capital_gain
        self.capital_loss = capital_loss
        self.hours_per_week = hours_per_week
        self.native_country = native_country.strip()
        self.predicted_value = predicted_value.strip()

    def __repr__(self):
        """Display when printing a Adult object"""

        pass

    def as_dict(self):
        """Convert object to dictionary"""

        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
