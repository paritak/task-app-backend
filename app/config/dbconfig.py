POSTGRES_CONFIG = {
    'user': 'postgres',
    'password': 'postgres',
    'database-name': 'postgres',
    'host': '127.0.0.1',
    'port': '5432'
}

FILE_CONFIG = {
    'file_path': '/home/parita/workspace/personal/task_project/backend/app/data_files/adult.data.csv',
    'insert_batch_count': 100
}