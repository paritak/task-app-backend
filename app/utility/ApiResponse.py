from flask import jsonify

class ApiResponse:

    def __init__(self):
        self.status = ""
        self.data = None
        self.message = ""

    def success(self, data=None, message=""):
        self.status = "success"
        self.message = message
        self.data = data
        json_response = self.__dict__
        return json_response, 200

    def warning(self,  data=None, message=""):
        self.status = "warning"
        self.message = message
        self.data = data
        json_response = self.__dict__
        return json_response, 200

    def bad_request(self, data=None, message=""):
        self.status = "error"
        self.message = message
        self.data = data
        json_response = self.__dict__
        return json_response, 400

    def restricted(self,  data=None):
        self.status = "error"
        self.message = "not authorised"
        self.data = data
        json_response = self.__dict__
        return json_response, 401


