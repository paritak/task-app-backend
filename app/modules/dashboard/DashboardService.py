from app.models.Models import *
from sqlalchemy import func,and_, or_


def getSummaryDetails():
    data = dict()
    data["gender_distribution"] = {}
    query = db.session.query(Adults.sex, func.count(Adults.id)).group_by(Adults.sex)
    result = query.all()
    for each_row in result:
        data["gender_distribution"][each_row[0]] = each_row[1]

    data["relationship_distribution"] = []
    query = db.session.query(Adults.relationship, func.count(Adults.id)).group_by(Adults.relationship)
    result = query.all()
    for each_row in result:
        temp = dict()
        temp["relationship"] = each_row[0]
        temp["value"] = each_row[1]
        data["relationship_distribution"].append(temp)

    print(data)
    return data


def listDetails(limit=None, offset=None, filter_obj=None):
    limit = limit if limit is not None else 10
    offset = offset if offset is not None else 0
    filter_obj = filter_obj if filter_obj is not None else {}

    filter_length = len(filter_obj)
    if filter_length == 0:
        query = db.session.query(Adults).limit(limit).offset(offset)

        filtered_count = db.session.query(Adults).count()
    else:
        query = db.session.query(Adults)
        if "sex" in filter_obj:
            query = query.filter(Adults.sex.in_(filter_obj["sex"]))
        if "race" in filter_obj:
            query = query.filter(Adults.race.in_(filter_obj["race"]))
        if "relationship" in filter_obj:
            query = query.filter(Adults.relationship.in_(filter_obj["relationship"]))

        filtered_count = query.count()

        query = query.limit(limit).offset(offset)

    data = []
    result = query.all()
    for each_row in result:
        data.append(each_row.as_dict())

    # print(data)
    return data, filtered_count

