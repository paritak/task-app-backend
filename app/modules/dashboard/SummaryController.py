from flask_restful import Resource
from .DashboardService import *
from app.utility.ApiResponse import *
import traceback


class SummaryController(Resource):

    def get(self):
        try:
            response = getSummaryDetails()
            return ApiResponse().success(data=response)
        except Exception:
            print(traceback.format_exc())
            return ApiResponse().bad_request(message="something went wrong")
