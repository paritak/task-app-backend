from flask_restful import Resource, reqparse
from flask import request
import traceback
from app.utility.ApiResponse import *
from .DashboardService import *


class ListController(Resource):

    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('limit', type=int, location='args')
            parser.add_argument('offset', type=int, location='args')
            parser.add_argument('filter', type=dict, location='json')

            args = parser.parse_args(strict=True)
            print(args)

            data, count = listDetails(args['limit'], args['offset'], args['filter'])
            response = dict()
            response['list'] = data
            response['count'] = count
            return ApiResponse().success(data=response)
        except Exception:
            print(traceback.format_exc())
            return ApiResponse().bad_request(message="something went wrong")

