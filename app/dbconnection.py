
from .config.dbconfig import *
from .models.Models import *
from sqlalchemy.orm import backref, sessionmaker


username = POSTGRES_CONFIG['user']
password = POSTGRES_CONFIG['password']
database_name = POSTGRES_CONFIG['database-name']
host = POSTGRES_CONFIG['host']
port = POSTGRES_CONFIG['port']


def connect_to_db(app):

    SQLALCHEMY_DATABASE_URI = 'postgresql://'+username+':'+password+'@'+host+':'+port+'/'+database_name

    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.app = app
    db.init_app(app)

    # Create all tables if they don't already exist
    db.create_all()


def insert_from_file():
    session_obj = db.session
    adults_count = session_obj.query(Adults).count()

    if adults_count == 0:

        import csv
        reader = csv.DictReader(open(FILE_CONFIG['file_path'], 'rt'))

        objects = []
        count = 0
        for line in reader:
            objects.append(Adults(**line))

            count += 1

            if count % FILE_CONFIG['insert_batch_count'] == 0:
                session_obj.bulk_save_objects(objects)
                session_obj.commit()
                objects = []

        if len(objects) > 0:
            session_obj.bulk_save_objects(objects)
            session_obj.commit()



